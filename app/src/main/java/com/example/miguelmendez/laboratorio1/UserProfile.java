package com.example.miguelmendez.laboratorio1;

import android.net.Uri;
import android.widget.ImageView;
import android.widget.RadioGroup;

/**
 * Created by MiguelMendez on 04/04/2018.
 */

public class UserProfile {
    public String name;
    public String profile;
    public String sexo;
    public Uri image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Uri getImage() {
        return image;
    }

    public void setImage(Uri image) {
        this.image = image;
    }





}
