package com.example.miguelmendez.laboratorio1;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int IMAGE_CAPTURE = 101;
    Uri uriImage;

    private static final int REQUEST_CODE_VOICE = 1234;
    TextView startRecordingButton;

    List<UserProfile> listaUsuarios=new ArrayList<>();
    UserProfileAdapter adapter=null;

    EditText name;
    EditText profile;
    RadioGroup types;
    public void startRecording(View viw){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, IMAGE_CAPTURE);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == IMAGE_CAPTURE){
            if(resultCode == RESULT_OK){
                uriImage = data.getData();
                Toast.makeText(this, "Excellent!", Toast.LENGTH_LONG).show();
            }
            else if(resultCode == RESULT_CANCELED){
                Toast.makeText(this, "Video recording cancelled. ", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(this, "Failed to record video", Toast.LENGTH_LONG).show();
            }
        }
        else if(requestCode == REQUEST_CODE_VOICE && resultCode== RESULT_OK ) {
            ArrayList<String> speech = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String strSpeech2Text = speech.get(0);
            startRecordingButton.setText(strSpeech2Text);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button recordButton = findViewById(R.id.recordButton);
        startRecordingButton = findViewById(R.id.profile);
        Button save = findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(existImage()){
                    UserProfile r=new UserProfile();
                    name=findViewById(R.id.name);
                    r.setName(name.getText().toString());
                    profile=findViewById(R.id.profile);
                    r.setProfile(profile.getText().toString());
                    types=findViewById(R.id.types);
                    switch(types.getCheckedRadioButtonId()){
                        case R.id.male:
                            r.setSexo("Male");
                            break;
                        case R.id.female:
                            r.setSexo("Female");
                            break;
                    }
                    r.setImage(uriImage);
                    uriImage = null;
                    adapter.add(r);
                }

            }
        });
        adapter= new UserProfileAdapter();
        ListView listView = findViewById(R.id.listViewProfile);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });

        startRecordingButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v) {
                if(isConnected()) {
                    Intent intent = new Intent (RecognizerIntent.ACTION_RECOGNIZE_SPEECH ) ;
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-MX");
                    try {
                        startActivityForResult (intent , REQUEST_CODE_VOICE ) ;
                    } catch (ActivityNotFoundException a) {
                        Toast.makeText(getApplicationContext(), "Tú dispositivo no soporta el reconocimiento por voz", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getApplicationContext(), " Please Connect to Internet ", Toast.LENGTH_SHORT ).show();
                }
            }
        });
        if(!hasCamera())
            recordButton.setEnabled(false);
    }

    public boolean existImage(){
        if(uriImage==null){
            Toast.makeText(this, "First, you must take a picture ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean hasCamera(){
        return(getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY));
    }

    public boolean isConnected () {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE) ;
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net != null && net.isAvailable() && net.isConnected()) {
            return true ;
        } else {
            return false ;
        }
    }

    class UserProfileAdapter extends ArrayAdapter<UserProfile> {
        UserProfileAdapter() {
            super(MainActivity.this, R.layout.rows, listaUsuarios);
        }
        public View getView(int position, View convertView, ViewGroup parent){
            View row=convertView;
            UserProfileAdapterHolder holder=null;
            if(row==null){
                LayoutInflater inflater=getLayoutInflater();
                row=inflater.inflate(R.layout.rows, parent,false);
                holder=new UserProfileAdapterHolder(row);
                row.setTag(holder);
            }
            else{
                holder=(UserProfileAdapterHolder) row.getTag();
            }
            holder.populateFrom(listaUsuarios.get(position));
            return (row);
        }
    }

    static class UserProfileAdapterHolder{
        private TextView name=null;
        private TextView profile=null;
        private TextView sexo = null;
        private ImageView icon=null;
        UserProfileAdapterHolder(View row){
            name=row.findViewById(R.id.nameRow);
            profile=row.findViewById(R.id.profileRow);
            sexo=row.findViewById(R.id.sexoRow);
            icon= row.findViewById(R.id.iconRow);
        }
        void populateFrom(UserProfile r) {
            icon.setImageURI(r.getImage());
            name.setText("Name: " + r.getName());
            profile.setText("Profile: " + r.getProfile());
            sexo.setText("Sex: " + r.getSexo());

        }
    }

}
